NAME
  qflow - Open-Source Digital Synthesis Flow

SYNOPSIS
  qflow [processes] [options] <module_name>

DESCRIPTION
 This is the main executable for a complete tool chain
 for synthesizing digital circuits starting
 from verilog source and ending in physical layout for a specific target
 fabrication process. In the world of commercial electronics, digital
 synthesis with a target application of a chip design is usually bundled
 into large EDA software systems. As commercial electronics designers need
 to maintain cutting-edge performance, these commercial toolchains get more
 and more expensive, and have largely priced themselves out of all but the
 established integrated circuit manufacturers. This leaves an unfortunate
 gap where startup companies and small businesses cannot afford to do any
 sort of integrated circuit design.
  
 Qflow tries to fill this gap.
  
PROCESSES
 synthesize         Synthesize verilog source
 place              Run initial placement
 sta                Static timing analysis
 route              Run placement and route
 decongest          Run congestion analysis, final place and route
 clean              Remove temporary working files
 display            Display routed result
 
 build              Run scripts synthesize to route
 all                Run scripts synthesize to display

OPTIONS
  -T, --tech <name>           Use technology <name>  (Default: osu035)
  -p, --project <name>       Project root directory is <name>

USAGE
 The simplest way to use qflow is to be in a directory with one Verilog file called input.v and execute:

   qflow build input.v

 This will run all the necessary steps in order to produce a layout of the specified Verilog file. If only certain steps are desired, one may first run:

   qflow input.v

 and then open qflow_exec.sh and uncomment the desired steps, and at last run:

   ./qflow_exec.sh

 It is also possible to change other "project-specific" details in the files qflow_vars.sh and project_vars.sh which also have been created in the current directory.

TECHNOLOGY
 qflow looks for technology files under /usr/share/qflow/tech. A shell script must be placed in a sub directory there declaring where each of the technology files are located:

  /usr/share/qflow/tech/[TECHNAME]/[TECHNAME].sh

 Usually, the files needed are placed in the following locations:

  /usr/share/qflow/tech/[TECHNAME]/SCN4M_SUBM.20.tech
  /usr/share/qflow/tech/[TECHNAME]/[TECHNAME].magicrc
  /usr/share/qflow/tech/[TECHNAME]/[TECHNAME].prm
  /usr/share/qflow/tech/[TECHNAME]/[TECHNAME]_stdcells.v
  /usr/share/qflow/tech/[TECHNAME]/[TECHNAME]_stdcells.lef
  /usr/share/qflow/tech/[TECHNAME]/[TECHNAME].par
  /usr/share/qflow/tech/[TECHNAME]/[TECHNAME]_stdcells.sp
  /usr/share/qflow/tech/[TECHNAME]/[TECHNAME]_stdcells.lib

 If no technology name is specified on the command line with the -T/--tech option, it will attempt to use the default technology which is "osu035".

 If the technology files are located somewhere else, this can be specified in qflow_vars.sh which is created when running qflow. The variables "techdir" and "techname" must be changed accordingly.

AUTHOR
  This manual page was written by Ruben Undheim <ruben.undheim@gmail.com> for the Debian project (and may be used by others).
