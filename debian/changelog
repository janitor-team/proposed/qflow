qflow (1.4.64+dfsg.1-1~exp1) UNRELEASED; urgency=medium

  * New upstream release

 -- Ruben Undheim <ruben.undheim@gmail.com>  Mon, 11 Nov 2019 23:32:26 +0000

qflow (1.4.62+dfsg.1-1~exp1) experimental; urgency=low

  * New upstream release

 -- Ruben Undheim <ruben.undheim@gmail.com>  Sat, 19 Oct 2019 21:13:08 +0200

qflow (1.3.17+dfsg.1-1) unstable; urgency=medium

  * Upload to sid
  * New upstream release
  * debian/control:
    - New standards version 4.4.1 - no changes
    - Use debhelper-compat build-dependency instead of debian/compat file
    - DH level 12

 -- Ruben Undheim <ruben.undheim@gmail.com>  Sat, 19 Oct 2019 11:10:52 +0200

qflow (1.3.12+dfsg.1-1~exp1) experimental; urgency=medium

  * New upstream release
  * debian/control:
    - New standards version 4.3.0 - no changes

 -- Ruben Undheim <ruben.undheim@gmail.com>  Sat, 16 Mar 2019 11:43:48 +0100

qflow (1.1.121+dfsg.1-2) unstable; urgency=medium

  * d/patches/0006-Cherry-pick-file-list-feature-from-development-minor.patch:
    - Support for file lists pointing to many source Verilog files.
      Cherry-picked from 1.2.x series
  * Fix spelling error in changelog

 -- Ruben Undheim <ruben.undheim@gmail.com>  Sun, 14 Oct 2018 10:00:09 +0200

qflow (1.1.121+dfsg.1-1) unstable; urgency=medium

  * New upstream release
  * Bump debhelper to 11
  * debian/control:
    - New standards version 4.2.1 - no changes
    - Remove unnecessary build-dependencies
    - Vcs URL updated to salsa
    - Recommends: python3-tk
    - Suggests: netgen-lvs - not yet in Debian
  * debian/copyright:
    - Update copyright years for upstream
    - Exclude some files which may not be DFSG compliant when repackaging.
    - Use https URL for format field
  * debian/patches: Refreshed all patches
  * debian/patches/0005-Remove-references-to-excluded-files.patch:
    - Remove all references to excluded files during repackaging
  * debian/patches/01_fix_install_dir.patch:
    - Adapt new scripts to the Debian specific location of files
  * debian/patches/fix_tcsh_path.patch:
    - Fix tcsh patch for one more script
  * debian/source.lintian-overrides removed
  * debian/qflow.docs: Install qflow_help.txt
  * debian/qflow.links: Link to /usr/bin/netgen-lvs
  * debian/rules:
    - Remove '--with autotools_dev'
    - Ensure dh_clean properly cleans up
    - Do not install qflow_help.txt in scripts directory
  * debian/tests:
    - Added simple autopkgtest for checking if 'qflow -h' can be run
  * debian/watch:
    - Mangle +dfsg
    - Only look for releases with stable minor number (1.1.x)

 -- Ruben Undheim <ruben.undheim@gmail.com>  Sun, 23 Sep 2018 11:00:52 +0200

qflow (1.1.58-1) unstable; urgency=medium

  * New upstream release
  * Upload to unstable
  * Refreshed patches
  * debian/control:
    - New standards version 4.0.0 - no changes
    - Depend on python3
  * debian/patches/0005-Fix-shebang.patch:
    - Fixing shebang issue in a new Python script

 -- Ruben Undheim <ruben.undheim@gmail.com>  Thu, 13 Jul 2017 22:29:51 +0200

qflow (1.1.44-1~exp1) experimental; urgency=low

  * New upstream release
  * debian/copyright:
    - Added copyright info for some new files

 -- Ruben Undheim <ruben.undheim@gmail.com>  Fri, 17 Mar 2017 13:54:14 +0100

qflow (1.1.37-1) unstable; urgency=medium

  * New upstream release
    - Refreshed patches
  * debian/control:
    - Standards version 3.9.8 - no changes

 -- Ruben Undheim <ruben.undheim@gmail.com>  Thu, 06 Oct 2016 18:38:30 +0200

qflow (1.1.31-1) unstable; urgency=medium

  * New upstream release
  * debian/control:
    - Standards version 3.9.7 - no required changes
    - Vcs-Git now using https
  * debian/patches:
    - Refreshed patches

 -- Ruben Undheim <ruben.undheim@gmail.com>  Wed, 13 Apr 2016 20:35:54 +0200

qflow (1.1.23-1) unstable; urgency=medium

  * New upstream release

 -- Ruben Undheim <ruben.undheim@gmail.com>  Sun, 29 Nov 2015 20:30:53 +0100

qflow (1.1.21-1) unstable; urgency=low

  * New upstream release
    - Refreshed patches

 -- Ruben Undheim <ruben.undheim@gmail.com>  Mon, 16 Nov 2015 19:12:14 +0100

qflow (1.1.13-2) unstable; urgency=medium

  * Added patch debian/patches/yosys_version_number_minor.patch:
    - Make sure qflow works with the new git HEAD version of yosys

 -- Ruben Undheim <ruben.undheim@gmail.com>  Sun, 11 Oct 2015 23:08:55 +0200

qflow (1.1.13-1) unstable; urgency=low

  * New upstream release
  * debian/man/qflow.txt:
    - Added much more detailed info to man page.

 -- Ruben Undheim <ruben.undheim@gmail.com>  Sun, 11 Oct 2015 00:02:24 +0200

qflow (1.1.9-1) unstable; urgency=low

  * Initial release (Closes: #761368)

 -- Ruben Undheim <ruben.undheim@gmail.com>  Sat, 25 Jul 2015 16:01:03 +0200
